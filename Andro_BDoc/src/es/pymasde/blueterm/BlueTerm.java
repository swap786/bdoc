package es.pymasde.blueterm;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.TextView;
import android.widget.EditText;  
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class BlueTerm extends Activity
{
TextView pulse;
TextView temp;
TextView posture;

EditText myTextbox;
BluetoothAdapter mBluetoothAdapter;
BluetoothSocket mmSocket;
BluetoothDevice mmDevice;
OutputStream mmOutputStream;
InputStream mmInputStream;
Thread workerThread;
byte[] readBuffer;
int readBufferPosition;
int counter;
volatile boolean stopWorker;
private Button openButton;
//private Button sendButton;
private EditText mobile;
private Button closeButton;
private String Temperature;
private String Posture;
private String Pulse;

@Override
public void onCreate(Bundle savedInstanceState)
{
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    openButton = (Button)findViewById(R.id.open);
    mobile = (EditText)findViewById(R.id.editText1);
    //sendButton = (Button)findViewById(R.id.send);
    closeButton = (Button)findViewById(R.id.close);
    pulse = (TextView)findViewById(R.id.label);
    temp = (TextView)findViewById(R.id.textView1);
    posture = (TextView)findViewById(R.id.textView2);
    //myTextbox = (EditText)findViewById(R.id.entry);

    //Open Button
    openButton.setOnClickListener(new View.OnClickListener()
    {
        public void onClick(View v)
        {
            try 
            {
            	// If BT is not on, request that it be enabled.
        		// setupChat() will then be called during onActivityResult
        		if (!mBluetoothAdapter.isEnabled()) {
        			Intent enableIntent = new Intent(
        					BluetoothAdapter.ACTION_REQUEST_ENABLE);
        			startActivityForResult(enableIntent, 3);
        			// Otherwise, setup the chat session
        		} else {
        			 findBT();
                     openBT();
        		}
               
            }
            catch (IOException ex) { }
        }
    });

    //Send Button
    /*sendButton.setOnClickListener(new View.OnClickListener()
    {
        public void onClick(View v)
        {
            try 
            {
                sendData();
            }
            catch (IOException ex) { }
        }
    });*/

    //Close button
    closeButton.setOnClickListener(new View.OnClickListener()
    {
        public void onClick(View v)
        {
            try 
            {
                closeBT();
            }
            catch (IOException ex) { }
        }
    });
}

void findBT()
{
    
    if(mBluetoothAdapter == null)
    {
        //myLabel.setText("No bluetooth adapter available");
    }

    if(!mBluetoothAdapter.isEnabled())
    {
        Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBluetooth, 0);
    }

    Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
    if(pairedDevices.size() > 0)
    {
        for(BluetoothDevice device : pairedDevices)
        {
            if(device.getName().equals("HC-05")) 
            {
                mmDevice = device;
                break;
            }
        }
    }
//    /myLabel.setText("Bluetooth Device Found");
}

void openBT() throws IOException
{
    UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //Standard SerialPortService ID
    mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);        
    mmSocket.connect();
    mmOutputStream = mmSocket.getOutputStream();
    mmInputStream = mmSocket.getInputStream();

    beginListenForData();

    //myLabel.setText("Bluetooth Opened");
}

void beginListenForData()
{
    final Handler handler = new Handler(); 
    final byte delimiter = 10; //This is the ASCII code for a newline character

    stopWorker = false;
    readBufferPosition = 0;
    readBuffer = new byte[1024];
    workerThread = new Thread(new Runnable()
    {
        public void run()
        {                
           while(!Thread.currentThread().isInterrupted() && !stopWorker)
           {
                try 
                {
                    int bytesAvailable = mmInputStream.available();                        
                    if(bytesAvailable > 0)
                    {
                        byte[] packetBytes = new byte[bytesAvailable];
                        mmInputStream.read(packetBytes);
                        for(int i=0;i<bytesAvailable;i++)
                        {
                            byte b = packetBytes[i];
                            if(b == delimiter)
                            {
     byte[] encodedBytes = new byte[readBufferPosition];
     System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
     final String data = new String(encodedBytes, "US-ASCII");
     readBufferPosition = 0;

                                handler.post(new Runnable()
                                {
                                    public void run()
                                    {//H 56#T89.56*PH$
                                        //myLabel.setText(data);
                                        if(data.contains("H ") && data.contains("#T") && data.contains("*P") && data.contains("$"))
                                        {
                                        	Pulse = data.substring(data.indexOf("H "),data.indexOf("#")).substring(2).trim();
                                        	Temperature = data.substring(data.indexOf("#T"),data.indexOf("*")).substring(2).trim();
                                        	Posture = data.substring(data.indexOf("*P"),data.indexOf("$")).substring(2).trim();
                                        	//Toast.makeText(getApplicationContext(), "Pulse="+Pulse+"\nPos="+Posture+"\nTemp="+Temperature, 
                          					  //    Toast.LENGTH_SHORT).show();	
                                        	pulse.setText("Pulse : "+Pulse);
                                        	temp.setText("Temperature : "+Temperature);
                                        	posture.setText("Posture : "+Posture);
                                        	 String str = null;
                                        	 if(Integer.parseInt(Pulse) > 110 || Integer.parseInt(Pulse) < 30 || Float.parseFloat(Temperature) > 38.00){
                         					
                                        		 try {
                                     				SmsManager smsManager = SmsManager.getDefault();
                                     				smsManager.sendTextMessage(mobile.getText().toString(), null, "pid=99"+"&pulse="+Pulse+"&temp="+Temperature+"&pos="+Posture, null, null);
                                     				Toast.makeText(getApplicationContext(), "SMS Sent!",
                                     							Toast.LENGTH_LONG).show();
                                     			  } catch (Exception e) {
                                     				Toast.makeText(getApplicationContext(),
                                     					"SMS faild, please try again later!",
                                     					Toast.LENGTH_LONG).show();
                                     				e.printStackTrace();
                                     			  }
                                        		 
                                        		 try {
                         						str = new DownloadFilesTask().execute("pid=99"+"&pulse="+Pulse+"&temp="+Temperature+"&pos="+Posture, "", "").get();
                         						Toast.makeText(getApplicationContext(), str, 
                                					      Toast.LENGTH_SHORT).show();	
                         					} catch (InterruptedException e) {
                         						e.printStackTrace();
                         					} catch (ExecutionException e) {
                         						e.printStackTrace();
                         					}
                                        }
                                        }
                                    }
                                });
                            }
                            else
                            {
                                readBuffer[readBufferPosition++] = b;
                            }
                        }
                    }
                } 
                catch (IOException ex) 
                {
                    stopWorker = true;
                }
           }
        }
    });

    workerThread.start();
}

void sendData() throws IOException
{
    String msg = myTextbox.getText().toString();
    msg += "\n";
    mmOutputStream.write(msg.getBytes());
    //myLabel.setText("Data Sent");
}

void closeBT() throws IOException
{
    stopWorker = true;
    mmOutputStream.close();
    mmInputStream.close();
    mmSocket.close();
    //myLabel.setText("Bluetooth Closed");
}
}

class DownloadFilesTask extends AsyncTask<String , String, String>
{
	protected String doInBackground(String... msg)
     {
		String response = "";

		try{
			String url = "http://doc-swappy.rhcloud.com/doc/put?"+msg[0];
			 
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	 
			// optional default is GET
			con.setRequestMethod("GET");
	 
			//add request header
			con.setRequestProperty("User-Agent", "Mozilla/5.0");
	 
			int responseCode = con.getResponseCode();
	
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
	 
			while ((inputLine = in.readLine()) != null) {
				response+=inputLine;
			}
			in.close();
	 
	 	}
			catch (Exception e) {
			//return e.toString();
			}
         return response;
     }
 
     @Override
     protected void onProgressUpdate(String... progress)
     {
        // setProgressPercent(progress[0]);
     }
 
     @Override
     protected void onPostExecute(String result) {
        // showDialog("Downloaded " + result + " bytes");
     }
 }
